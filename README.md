# Equipe de desenvolvimento do Cliente

## Apresentação:
* Necessário desenvolver um cliente através do node.js, onde o este será responsável pela comunicação, tais como mensagem de vitória ou derrota e mensagens para o jogador.Uma das maiores competências será a criação de um bot totalmente automatizado, que jogará partidas quando o jogador selecionar a opção de singleplayer.

## Objetivos:
* Desenvolver a biblioteca cliente TCP/tetrinet


## Grupo 6 - membros
* Karlla Chaves (Developer)
* Marcos Vinícius de Souza Oliveira (Líder)
* Luan Oliveira (Developer)
* Luis Eduardo Peixoto de Aquino (Developer)
* Cardeque Henrique (Developer)


