const net = require ('net')

const client =  net.createConnection({port: 31457}, () =>{
    console.log('Client connected to server');
});

client.on('data',(data)=>{
    console.log(data.toString());
    client.end();
});

client.on('end',()=>{
    console.log('Client disconnected from server');
});

function loginMessageEncode(){
    var ip = [209,52,144,98];
    login_message = 'tetrifaster TestUser 1.13';
    h = String(54 * ip[0] + 41 * ip[1] + 29 * ip[2] + 17 * ip[3]);
    dec = Math.floor((Math.random() * 256) + 1); //rand number  0 <= x <= 256
    encoded = dec.toString(16);
    for (var i=0 ; i < login_message.length; i++){
        var charToUnicode = login_message[i].charCodeAt(0);
        dec = ((dec + charToUnicode) % 255) ^ h[i % h.length].codePointAt(0);
        encoded += dec.toString(16);
    }
    console.log("Login message encoded:" + encoded);
    return encoded;
}


